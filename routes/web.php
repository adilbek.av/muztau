<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/track', 'HomeController@track')->name('track')->middleware('auth');
Route::get('/services/{id}', 'HomeController@service')->name('service');
Route::post('/application', 'HomeController@application')->name('application');
Route::get('/{slug}', 'HomeController@serviceSlug')->name('service.slug');
