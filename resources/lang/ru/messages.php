<?php

return [
    'locale' => 'Русский',
    'login' => 'Вход',
    'links' => [
        'about' => 'О нас',
        'railway' => 'ЖД транспорт',
        'auto' => 'Авто транспорт',
        'contacts' => 'Контакты'
    ],
    'wrapper' => 'Мы - единая команда по всему миру<br /> отвечающая за вашу цепочку поставку и логистику.<br /> Наша глобальная сервисная сеть позволяет нам обслуживать любой ваш запрос. Мы там, где нужно нашему клиенту. Мы делаем все, что пожелает наш клиент',
    'about' => [
        'title' => 'О нас',
        'text' => 'Международная транспортно-экспедиторская компания Muztau Logistics принимаются заказы на международные перевозки четырьмя видами транспорта. Машины, самолеты, морские суда и поезда выполняют оперативную доставку грузов в любую точку мира. Стоимость услуг одна из самых низких. Это объясняется грамотным подходом к логистике и правильной ценовой политикой.',
        'list_title' => 'Осуществляем следующие перевозки и услуги',
        'list' => [
            'Тяжеловесные и крупногабаритные. Используем специальный транспорт для выполнения сложных задач.',
            'Сборные. Перевозка сборных грузов - один из самых дешевых и популярных видов транспортировки.',
            'Проектные. Нестандартные грузы, которые требуют креативного подхода.',
            'Консолидация. Собираем и временно храним товары до дальнейшей отправки в составе сборного груза.',
            'Хранение. Располагаем большими и безопасными складами, где грузы хранятся в полной сохранности.',
            'Оформление. Составляем таможенные и страховые документы, необходимые для сопровождения груза.'
        ]
    ],
    'services' => [
        'title' => 'Наши услуги'
    ],
    'dislocation' => [
        'title' => 'Дислокация заказа',
        'helper_text' => 'для отслеживание груза'
    ],
    'application' => [
        'title' => 'Оставьте заявку сейчас',
        'helper_text' => 'и получите уникальные условия сотрудничества'
    ],
    'clients' => [
        'title' => 'Наши клиенты и партнёры'
    ],
    'benefits' => [
        'title' => 'Наши преимущества'
    ],
    'contacts' => [
        'title' => 'Контакты',
        'subtitle' => 'Нужен совет?',
        'helper_text' => 'Оставьте свой номер и наши консультанты помогут Вам с выбором!'
    ],
    'more' => 'Подробнее',
    'email' => 'E-mail',
    'password' => 'Пароль',
    'name' => 'Имя',
    'phone' => 'Телефон',
    'message' => 'Текст сообщения',
    'submit' => 'Отправить',
    'application_success' => 'Заявка успешно отправлена!',
    'tracking' => 'Отслеживание',
    'log_out' => 'Выйти',
    'track' => [
        'container' => 'Номер контейнера/вагона',
        'cis' => 'Номер вагона CIS',
        'border_station' => 'Пограничная станция',
        'reloading_date' => 'Дата перезарядки',
        'fd_station' => 'К/С станция',
        'current_location' => 'Текущее местоположение',
        'km_till' => 'KM до к/с',
        'comment' => 'Комментарий',
        'arrival_date' => 'Дата прибытия'
    ],
    'search' => 'Поиск'
];
