<?php

return [
    'locale' => '中文',
    'login' => '登入',
    'links' => [
        'about' => '关于我们',
        'railway' => '铁路运输',
        'auto' => '汽车运输',
        'contacts' => '联络人'
    ],
    'wrapper' => '我们是一个由全球组成的团结团队<br />，负责您的供应链和物流。 <br />我们的全球服务网络使我们能够满足您的任何要求。 我们是客户需要的地方。 我们做客户希望的一切。',
    'about' => [
        'title' => '关于我们',
        'text' => 'Muztau Logistics 专门从事欧洲，中国和独联体地区之间的铁路和多式联运服务。 我们在独联体，欧洲和中国的铁路网络以及货运代码系统方面的丰富经验和深入的知识，使我们能够及时，专业地应对客户的各种复杂和复杂的铁路和联运运输要求。',
        'list_title' => '我们进行以下运输和服务',
        'list' => [
            '沉重和超大。我们使用特殊的车辆执行复杂的任务。',
            '国家队。分组货物运输是最便宜和最受欢迎的运输类型之一。',
            '设计。需要创新方法的非标商品。',
            '合并。我们会收集并临时存储货物，直到作为打包货物的一部分进一步发送为止。',
            '存储。我们有大而安全的仓库，在这里完全安全地存储了货物。',
            '注册。我们起草了货物随附的海关和保险单据。'
        ]
    ],
    'services' => [
        'title' => '我们的服务'
    ],
    'dislocation' => [
        'title' => '订单位置',
        'helper_text' => '用于货物追踪'
    ],
    'application' => [
        'title' => '立即提出要求',
        'helper_text' => '并获得独特的合作条款'
    ],
    'clients' => [
        'title' => '我们的客户和合作伙伴'
    ],
    'benefits' => [
        'title' => '我们的优势'
    ],
    'contacts' => [
        'title' => '联络人',
        'subtitle' => '需要一些建议吗？',
        'helper_text' => '留下您的电话号码，我们的顾问将帮助您选择！'
    ],
    'more' => '更多细节',
    'email' => 'E-mail',
    'password' => '密码',
    'name' => '名称',
    'phone' => '电话',
    'message' => '讯息文字',
    'submit' => '发送',
    'application_success' => '申请已成功发送！',
    'tracking' => '追踪',
    'log_out' => '登出',
    'track' => [
        'container' => '货柜/货车编号',
        'cis' => 'CIS 货车编号',
        'border_station' => '边境站',
        'reloading_date' => '装货日期',
        'fd_station' => 'F/D 站',
        'current_location' => '当前位置',
        'km_till' => 'KM 直到 F/D',
        'comment' => '评论',
        'arrival_date' => '入住日期'
    ],
    'search' => '搜索'
];
