<?php

return [
    'locale' => 'English',
    'login' => 'Sign in',
    'links' => [
        'about' => 'About us',
        'railway' => 'Railway transport',
        'auto' => 'Auto transport',
        'contacts' => 'Contacts'
    ],
    'wrapper' => 'We are a united team worldwide <br /> responsible for your supply chain and logistics. <br /> Our global service network allows us to service any of your requests. We are where our client needs. We do everything our client wishes.',
    'about' => [
        'title' => 'About us',
        'text' => 'Muztau Logistics specializes in Railway and Intermodal Transportation Services between Europe, China and the CIS regions. Our vast experience and in-depth knowledge of the CIS, Europe and Chinese railway networks and freight payment code systems, enables us to respond timely and professionally to all kinds of intricate and complex railway and intermodal transportation requirements of our customers.',
        'list_title' => 'We carry out the following transportation and services',
        'list' => [
            'Heavy and oversized. We use special vehicles to perform complex tasks.',
            'National teams. Groupage cargo transportation is one of the cheapest and most popular types of transportation.',
            'Design. Non-standard goods that require a creative approach.',
            'Consolidation. We collect and temporarily store goods until further dispatch as part of a groupage cargo.',
            'Storage. We have large and safe warehouses where the goods are stored in full safety.',
            'Registration. We draw up customs and insurance documents necessary to accompany the cargo.'
        ]
    ],
    'services' => [
        'title' => 'Our services'
    ],
    'dislocation' => [
        'title' => 'Order Location',
        'helper_text' => 'for cargo tracking'
    ],
    'application' => [
        'title' => 'Leave a request now',
        'helper_text' => 'and get unique terms of cooperation'
    ],
    'clients' => [
        'title' => 'Our customers and partners'
    ],
    'benefits' => [
        'title' => 'Our advantages'
    ],
    'contacts' => [
        'title' => 'Contacts',
        'subtitle' => 'Need some advice?',
        'helper_text' => 'Leave your number and our consultants will help you with a choice!'
    ],
    'more' => 'More details',
    'email' => 'E-mail',
    'password' => 'Password',
    'name' => 'Name',
    'phone' => 'Phone',
    'message' => 'Message text',
    'submit' => 'Send',
    'application_success' => 'Application sent successfully!',
    'tracking' => 'Tracking',
    'log_out' => 'Log out',
    'track' => [
        'container' => 'Container/wagon number',
        'cis' => 'CIS wagon number',
        'border_station' => 'Border station',
        'reloading_date' => 'Reloading date',
        'fd_station' => 'F/D station',
        'current_location' => 'Current location',
        'km_till' => 'KM till f/d',
        'comment' => 'Comment',
        'arrival_date' => 'Date of arrival'
    ],
    'search' => 'Search'
];
