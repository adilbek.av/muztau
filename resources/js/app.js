require('./bootstrap');

(() => {
    'use strict';
    // Page is loaded
    const objects = document.getElementsByClassName('asyncImage');
    Array.from(objects).map((item) => {
        // Start loading image
        const img = new Image();
        img.src = item.dataset.src;
        // Once image is loaded replace the src of the HTML element
        img.onload = () => {
            item.classList.remove('asyncImage');
            return item.nodeName === 'IMG' ?
                item.src = item.dataset.src :
                item.style.backgroundImage = `url(${item.dataset.src})`;
        };
    });

    $('#toastMessage').toast('show')
})();

$.fn.isInViewport = function () {
    let elementTop = $(this).offset().top;
    let elementBottom = elementTop + $(this).outerHeight();
    let viewportTop = $(window).scrollTop();
    let viewportBottom = viewportTop + $(window).height();
    return elementBottom > viewportTop && elementTop < viewportBottom;
};

$(window).on('resize scroll', function () {
    let header = $('.navbar-hybrid');
    if ($(this).scrollTop() >= header.height()) {
        header.removeClass('navbar-dark').addClass('navbar-light').addClass('bg-light')
    } else {
        header.removeClass('navbar-light').removeClass('bg-light').addClass('navbar-dark')
    }

    // navbar
    let sections = ['about', 'service0', 'service1', 'contacts']

    sections.forEach(element => {
        let id = '#' + element
        let link = $('.nav-link[href="' + id + '"]')
        if (link.length) {
            if ($(id).isInViewport()) {
                link.parent().addClass('active')
            } else {
                link.parent().removeClass('active')
            }
        }
    });
});


$("header.header a").on('click', function (event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {

        // Prevent default anchor click behavior
        event.preventDefault();

        // Store hash
        let hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 800);

    } // End if

});
