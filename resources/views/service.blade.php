@extends('layouts.app', ['navbar' => 'navbar-light bg-light'])

@section('content')
    <section>
        <div id="wrapperMini"
             class="asyncImage" data-src="{{ asset('/storage/'.$service->top_image) }}"
             style="background-image: url('{{ Voyager::image($service->thumbnail('small', 'top_image')) }}')">
            <h1 class="display-4 text-white text-center text-uppercase font-weight-bold position-relative">Muztau<br/>Logistics
            </h1>
        </div>
        <article class="asyncImage" data-src="{{ asset('/storage/'.$service->bottom_image) }}"
             style="background-image: url('{{ Voyager::image($service->thumbnail('small', 'bottom_image')) }}')">
            <div class="container py-5">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <h2 class="title text-center h3">{{ $service->translate()->title }}</h2>
                        <p>{{ $service->translate()->excerpt }}</p>
                        <div class="content">
                            {!! $service->translate()->description !!}
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section>
@endsection
