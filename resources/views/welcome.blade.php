@extends('layouts.app', ['navbar' => 'navbar-hybrid navbar-dark fixed-top'])

@section('content')
    @include('partials.wrapper')
    @include('partials.about')
    @if($services->count())
        @include('partials.services')
    @endif
    @include('partials.dislocation')
    @include('partials.application')
    @if($clients->count())
        @include('partials.clients')
    @endif
    @if($benefits->count())
        @include('partials.benefits')
    @endif
@endsection
