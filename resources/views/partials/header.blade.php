<header class="header">
    <nav class="navbar navbar-expand-md {{ $navbar ?? 'navbar-dark' }}">
        <div class="container">
            <a class="navbar-brand ml-3 ml-md-0" href="{{ route('home') }}">
                <img src="{{ asset('storage/' . setting('site.logo')) }}" alt="{{ setting('site.title') }}">
            </a>
            <button class="navbar-toggler mr-3" type="button" data-toggle="collapse" data-target="#navbar"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbar">
                <ul class="navbar-nav justify-content-around" style="flex: 1;">
                    <li class="nav-item">
                        <a class="nav-link"
                           href="{{ request()->route()->getName() == 'home' ? '#about' : route('home') }}">{{ trans('messages.links.about') }}</a>
                    </li>
                    <li class="nav-item d-block d-md-none d-lg-block">
                        <a class="nav-link"
                           href="{{ route('service', ['id' => 2]) }}">{{ trans('messages.links.railway') }}</a>
                    </li>
                    <li class="nav-item d-block d-md-none d-lg-block">
                        <a class="nav-link"
                           href="{{ route('service', ['id' => 1]) }}">{{ trans('messages.links.auto') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contacts">{{ trans('messages.links.contacts') }}</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="localeDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{ asset('assets/icons/flags/'.app()->getLocale().'.svg') }}"
                                 alt="{{ trans('messages.locale') }}" class="mr-2 d-none d-md-inline">
                            {{ trans('messages.locale') }}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="localeDropdown">
                            @foreach(config('app.locales') as $locale)
                                @php
                                    $segments = request()->segments();
                                    if (count($segments) && in_array($segments[0], config('app.locales'))) {
                                        $tempLocale = array_shift($segments);
                                    }

                                    array_unshift($segments, $locale);
                                    $url= '/' . implode('/', $segments);
                                @endphp
                                <a class="dropdown-item" href="{{ $url }}">
                                    <img src="{{ asset('assets/icons/flags/'.$locale.'.svg') }}" alt="{{ $locale }}"
                                         class="mr-2">
                                    {{ trans('messages.locale', [], $locale) }}
                                </a>
                            @endforeach
                        </div>
                    </li>

                    @if(auth()->check())
                        <li class="nav-item dropdown nav-auth">
                            <a class="nav-link dropdown-toggle px-0 px-md-4 py-1"
                               href="#" role="button"
                               id="userDropdown"
                               data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false">{{ auth()->user()->email }}</a>

                            <div class="dropdown-menu" aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="{{ route('track') }}">
                                    {{ trans('messages.tracking') }}
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i
                                        class="lnr lnr-exit"></i> <span>{{ trans('messages.log_out') }}</span></a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @else
                        <li class="nav-item nav-auth">
                            <a class="nav-link px-0 px-md-4 py-1" data-toggle="modal" data-target="#loginModal"
                               href="javascript:void(0)">{{ trans('messages.login') }}</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
</header>
