<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content p-2">
            <div class="modal-body">
                <h5 class="card-title text-center text-primary font-weight-bold mb-0">{{ trans('messages.login') }}</h5>
                <span
                    class="d-block text-center text-primary mb-3">{{ trans('messages.dislocation.helper_text') }}</span>
                <form action="{{ route('login') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <input type="email" class="form-control" id="contactEmail" name="email"
                               aria-describedby="contactEmail" placeholder="{{ trans('messages.email') }}" required>
                        @if ($errors->has('email'))
                            <span class="text-danger small">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="contactPassword" name="password"
                               aria-describedby="contactPassword"
                               placeholder="{{ trans('messages.password') }}" required>
                        @if ($errors->has('password'))
                            <span class="text-danger small">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                    <button type="submit"
                            class="btn btn-warning w-100 text-white">{{ trans('messages.submit') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
