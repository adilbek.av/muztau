<section id="benefits" class="py-5 bg-primary">
    <div class="container">
        <h2 class="title text-center text-white mb-5">{{ trans('messages.benefits.title') }}</h2>
        <div class="row">
            @foreach($benefits as $item)
                <div class="col-md-6 col-lg-3">
                    <div class="benefit mb-4 mb-lg-0">
                        <img class="asyncImage d-block mx-auto mb-4" src="{{ Voyager::image($item->thumbnail('medium', 'icon')) }}"
                             data-src="/storage/{{$item->icon }}" alt="{{ $item->translate()->title }}">
                        <h6 class="text-uppercase text-warning font-weight-bold">{{ $item->translate()->title }}</h6>
                        <p class="text-white">{{ $item->translate()->description }}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
