<section id="clients" class="py-5">
    <div class="container">
        <h2 class="title text-center mb-3">{{ trans('messages.clients.title') }}</h2>
        <div class="d-flex flex-row flex-wrap">
            @foreach($clients as $item)
                <img class="client px-2 asyncImage" src="{{ Voyager::image($item->thumbnail('medium', 'cover')) }}"
                     data-src="/storage/{{$item->cover }}" alt="{{ $item->title }}">
            @endforeach
        </div>
    </div>
</section>
