<section id="about" class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-10 mx-auto">
                <h2 class="title text-center">{{ trans('messages.about.title') }}</h2>
                <p class="mb-0 text-center text-md-left">{{ trans('messages.about.text') }}</p>
            </div>
            <img class="asyncImage w-100 my-5" src="{{ asset('assets/images/about-min.png') }}"
                 data-src="{{ asset('assets/images/about.png') }}" alt="{{ trans('messages.about.title') }}">
            <div class="col-md-10 mx-auto">
                <h5 class="text-left text-primary font-weight-bold mb-3">{{ trans('messages.about.list_title') }}</h5>
                @if(trans('messages.about.list'))
                    <ul class="list-unstyled">
                        @foreach(trans('messages.about.list') as $item)
                            <li>{{ $item }}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
    </div>
</section>
