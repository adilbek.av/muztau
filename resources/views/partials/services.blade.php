<section id="services">
    <div class="container">
        <h2 class="title text-center mb-3">{{ trans('messages.services.title') }}</h2>
        @foreach($services as $index=>$item)
            <div class="service mb-4" id="service{{$index}}">
                <div class="d-flex flex-column justify-content-between">
                    <div>
                        <h4 class="text-primary font-weight-bold mb-3">{{ $item->translate()->title }}</h4>
                        <p class="pr-lg-3 pr-xl-5 mr-lg-3 mr-xl-5">{{ $item->translate()->excerpt }}</p>
                    </div>

                    <div class="mb-5">
                        <a href="{{ route('service.slug', ['slug' => $item->translate()->slug]) }}"
                           class="btn btn-outline-primary px-5">{{ trans('messages.more') }}</a>
                    </div>
                </div>
                <div>
                    @php($images = json_decode($item->images, JSON_PRETTY_PRINT))
                    @if(count($images) > 1)
                        <div id="serviceCarousel{{$index}}" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                @foreach($images as $imageIndex=>$image)
                                    <div class="carousel-item @if($imageIndex == 0) active @endif">
                                        <img class="asyncImage" src="{{ Voyager::image($item->getThumbnail($image, 'small')) }}"
                                             data-src="/storage/{{ $image }}" alt="{{ $item->translate()->title }}">
                                    </div>
                                @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#serviceCarousel{{$index}}" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#serviceCarousel{{$index}}" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    @else
                        <img class="asyncImage" src="{{ Voyager::image($item->getThumbnail($images[0], 'small')) }}"
                             data-src="/storage/{{ $images[0] }}" alt="{{ $item->translate()->title }}">
                    @endif
                </div>
            </div>
        @endforeach
    </div>
</section>
