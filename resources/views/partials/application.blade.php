<section id="application" class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-lg-5 mx-auto">
                <div class="card w-100 shadow-lg border-0">
                    <div class="card-body">
                        <h5 class="card-title text-center text-primary font-weight-bold  text-uppercase mb-3 mb-md-0">{{ trans('messages.application.title') }}</h5>
                        <span
                            class="d-none d-md-block text-center text-primary mb-3 text-nowrap">{{ trans('messages.application.helper_text') }}</span>
                        <form action="{{ route('application') }}" method="post">
                            @csrf
                            <input type="hidden" value="cooperation" name="type">
                            <div class="form-group">
                                <input type="text" class="form-control" id="applicationName" name="name"
                                       aria-describedby="applicationName" placeholder="{{ trans('messages.name') }}"
                                       required>
                                @if ($errors->application->has('name'))
                                    <span class="text-danger small">{{ $errors->application->first('name') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="applicationEmail" name="email"
                                       aria-describedby="applicationEmail" placeholder="{{ trans('messages.email') }}"
                                       required>
                                @if ($errors->application->has('email'))
                                    <span class="text-danger small">{{ $errors->application->first('email') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="applicationPhone" name="phone"
                                       aria-describedby="applicationPhone"
                                       placeholder="{{ trans('messages.phone') }}" required>
                                @if ($errors->application->has('phone'))
                                    <span class="text-danger small">{{ $errors->application->first('phone') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" id="applicationMessage" name="message"
                                          aria-describedby="applicationMessage"
                                          placeholder="{{ trans('messages.message') }}" required></textarea>
                                @if ($errors->application->has('message'))
                                    <span class="text-danger small">{{ $errors->application->first('message') }}</span>
                                @endif
                            </div>
                            <button type="submit"
                                    class="btn btn-warning w-100 text-white">{{ trans('messages.submit') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
