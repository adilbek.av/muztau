<section id="wrapper" class="asyncImage py-5" data-src="{{ asset('assets/images/main.png') }}">
    <div class="container">
        <h1 class="text-uppercase text-center text-md-right text-white h2 position-relative">{!! trans('messages.wrapper') !!}</h1>
    </div>
</section>
