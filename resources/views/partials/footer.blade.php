<footer class="footer" id="contacts">
    <div class="footer__top py-5">
        <h2 class="title text-white text-center mb-4">{{ trans('messages.contacts.title') }}</h2>
        <div class="container">
            <div class="row">
                <div class="offset-md-5 col-md-7 offset-lg-6 col-lg-6">
                    <h6 class="text-white font-weight-bold h5 mb-0">{{ trans('messages.contacts.subtitle') }}</h6>
                    <p class="text-white mb-4">{{ trans('messages.contacts.helper_text') }}</p>
                </div>
            </div>
            <div class="row flex-column-reverse flex-md-row">
                <div class="col-md-5 col-lg-6">
                    <ul class="list-unstyled mb-0 h-100 d-flex flex-column justify-content-between">
                        <li class="mb-3 mb-md-0">
                            <a href="mailto:{{setting('site.email')}}">
                                <img src="/assets/icons/email.svg" alt="Email">
                                {{setting('site.email')}}
                            </a>
                        </li>
                        <li class="mb-3 mb-md-0">
                            <a href="tel:{{setting('site.phone')}}">
                                <img src="/assets/icons/phone.svg" alt="Phone">
                                {{setting('site.phone')}}
                            </a>
                        </li>
                        <li>
                            <address>
                                <img src="/assets/icons/address.svg" alt="Address">
                                {{setting('site.address')}}
                            </address>
                        </li>
                    </ul>
                </div>
                <div class="col-md-7 col-lg-6">
                    <form action="{{ route('application') }}" method="post" class="mb-4 mb-md-0">
                        @csrf
                        <input type="hidden" value="tip" name="type">
                        <div class="form-group">
                            <input type="text" class="form-control" id="contactName" name="name"
                                   aria-describedby="contactName" placeholder="{{ trans('messages.name') }}" required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="contactPhone" name="phone"
                                   aria-describedby="contactPhone"
                                   placeholder="{{ trans('messages.phone') }}" required>
                        </div>
                        <button type="submit"
                                class="btn btn-warning w-100 text-white">{{ trans('messages.submit') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__bottom py-3">
        <img src="/assets/icons/logo2.png" alt="{{ setting('site.title') }}" class="footer__logo">
        <span class="m-0 text-white d-none d-sm-block">©2020 MUZTAU LOGISTIC. ALL RIGHTS RESERVED</span>
    </div>
</footer>
