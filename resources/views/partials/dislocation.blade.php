<section id="dislocation" class="py-5">
    <div class="container">
        <h2 class="title text-white text-center mb-4 position-relative">{{ trans('messages.dislocation.title') }}</h2>
        <div class="row">
            <div class="col-md-7 col-lg-5 mx-auto">
                <div class="card w-100 border-0">
                    <div class="card-body">
                        <h5 class="card-title text-center text-primary font-weight-bold mb-0">{{ trans('messages.login') }}</h5>
                        <span
                            class="d-block text-center text-primary mb-3">{{ trans('messages.dislocation.helper_text') }}</span>
                        <form action="{{ route('login') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <input type="email" class="form-control" id="dislocationEmail" name="email"
                                       aria-describedby="dislocationEmail" placeholder="{{ trans('messages.email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="text-danger small">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="dislocationPassword" name="password"
                                       aria-describedby="dislocationPassword"
                                       placeholder="{{ trans('messages.password') }}" required>
                                @if ($errors->has('password'))
                                    <span class="text-danger small">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                            <button type="submit"
                                    class="btn btn-warning w-100 text-white">{{ trans('messages.submit') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
