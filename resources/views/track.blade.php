@extends('layouts.app', ['navbar' => 'navbar-light bg-light'])

@section('content')
    <section id="track">
        <div id="wrapperMini"
             class="asyncImage" data-src="{{ asset('assets/images/main.png') }}"
             style="background-image: url('{{ asset('assets/images/main-min.png') }}')">
            <h1 class="display-4 text-white text-center text-uppercase font-weight-bold position-relative">Muztau<br/>Logistics
            </h1>
        </div>
        <div class="container pt-5">
            <div class="row">
                <div class="col-12">
                    <div class="media mb-4">
                        <img class="avatar rounded mr-3" src="{{ asset('/storage/'.auth()->user()->avatar) }}"
                             alt="{{ auth()->user()->email }}">
                        <div class="media-body">
                            <h5 class="mt-0 mb-3">{{ auth()->user()->name }}</h5>
                            @if(auth()->user()->phone)
                                <a href="tel:{{ auth()->user()->phone }}"
                                   class="d-block mb-3">{{ auth()->user()->phone }}</a>
                            @endif
                            <a href="mailto:{{ auth()->user()->email }}"
                               class="d-block mb-3">{{ auth()->user()->email }}</a>
                        </div>
                    </div>
                </div>
                <form action="" class="w-100">
                   <div class="form-group">
                       <div class="input-group">
                           <select class="form-control" name="column">
                               @foreach(trans('messages.track') as $key=>$item)
                                   <option value="{{ $key }}" @if(request()->get('column') == $key) selected @endif>{{ $item }}</option>
                               @endforeach
                           </select>
                           <input type="text" class="form-control" name="q" value="{{ request()->get('q') }}">
                           <div class="input-group-append">
                               <button class="btn btn-primary" type="submit">{{ trans('messages.search') }}</button>
                           </div>
                       </div>
                   </div>
                </form>
            </div>
        </div>
        <div class="container-fluid pb-5">
            <div class="table-responsive">
                <table class="table table-striped shadow rounded-lg mb-5">
                    <thead>
                    <tr>
                        @foreach(trans('messages.track') as $item)
                            <th scope="col" class="text-nowrap">{{ $item }}</th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tracks as $track)
                        <tr>
                            @foreach(trans('messages.track') as $key=>$item)
                                @if(in_array($key, ['border_station', 'fd_station', 'current_location', 'comment']))
                                    <td>{{ is_null($track->translate()[$key]) ? '/' : $track->translate()[$key] }}</td>
                                @else
                                    <td>{{ is_null($track[$key]) ? '/' : $track[$key] }}</td>
                                @endif
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            {{ $tracks->links() }}
        </div>
    </section>
@endsection
