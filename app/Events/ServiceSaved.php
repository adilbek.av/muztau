<?php

namespace App\Events;

use App\Service;
use Illuminate\Queue\SerializesModels;

class ServiceSaved
{
    use SerializesModels;

    public $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }
}
