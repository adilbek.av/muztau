<?php

namespace App\Http\Controllers;

use App\Application;
use App\Benefit;
use App\Client;
use App\Http\Resources\BenefitResource;
use App\Http\Resources\ClientResource;
use App\Http\Resources\ServiceResource;
use App\Service;
use App\Track;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{

    public function index()
    {
        $services = Service::select('id', 'title', 'excerpt', 'images', 'slug')->orderBy('order')->get();
        $clients = Client::select('id', 'name', 'cover')->orderBy('order')->get();
        $benefits = Benefit::select('id', 'title', 'description', 'icon')->orderBy('order')->get();

        return view('welcome')
            ->with('services', ServiceResource::collection($services))
            ->with('clients', ClientResource::collection($clients))
            ->with('benefits', BenefitResource::collection($benefits));
    }

    public function service($id)
    {
        $service = Service::whereId($id)->firstOrFail();
        return redirect()->route('service.slug', ['slug' => $service->slug]);
    }

    public function serviceSlug($slug)
    {
        $service = Service::whereTranslation('slug', '=', $slug)->first();


        if ($slug != $service->translate()->slug && $service->translate()->slug) {
            return redirect()->route('service.slug', ['slug' => $service->translate()->slug]);
        }

        return view('service')->with('service', new ServiceResource($service));
    }

    public function application()
    {

        Validator::make(request()->all(), [
            'name' => 'required|string|max:255',
            'phone' => 'required|between:10,20',
            'type' => 'required|in:cooperation,tip',
            'email' => 'required_if:type,cooperation|email',
            'message' => 'required_if:type,cooperation'
        ])->validateWithBag('application');

        Application::create(request()->only(['name', 'phone', 'type', 'email', 'message']));

        request()->session()->flash('message', trans('messages.application_success'));

        return redirect()->back();
    }

    public function track()
    {
        $tracks = Track::whereUserId(auth()->id())
            ->when(request()->get('column') && request()->get('q'), function ($query) {
                return $query->when(in_array(request()->get('column'), ['border_station', 'fd_station', 'current_location', 'comment']), function ($query) {
                    return $query->whereTranslation(request()->get('column'), 'LIKE', '%' . request()->get('q') . '%');
                }, function ($query) {
                    return $query->where(request()->get('column'), 'LIKE', '%' . request()->get('q') . '%');
                });
            })->orderByDesc('created_at')->paginate(15);
        return view('track')->with('tracks', $tracks);
    }
}
