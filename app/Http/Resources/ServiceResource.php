<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $translation = $this->translate();

        return [
            'id' => $this->id,
            'title' => $translation->title,
            'excerpt' => $translation->excerpt,
            'description' => $translation->description,
            'images' => collect(json_decode($this->images, JSON_PRETTY_PRINT)),
            'top_image' => $this->top_image,
            'bottom_image' => $this->bottom_image,
            'slug' => $translation->slug

        ];
    }
}
