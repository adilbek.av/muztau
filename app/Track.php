<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;


class Track extends Model
{
    use Translatable, SoftDeletes;
    protected $translatable = ['border_station', 'fd_station', 'current_location', 'comment'];
    protected $fillable = ['border_station', 'fd_station', 'current_location', 'comment', 'container', 'cis', 'reloading_date', 'arrival_date', 'km_till', 'arrival_date'];
    protected $hidden = ['updated_at', 'deleted_at'];
    protected $dates = ['deleted_at'];
}
