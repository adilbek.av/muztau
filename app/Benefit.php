<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Resizable;
use TCG\Voyager\Traits\Translatable;


class Benefit extends Model
{
    use Resizable, Translatable, SoftDeletes;
    protected $translatable = ['title', 'description'];
    protected $hidden = ['order', 'updated_at', 'deleted_at'];
    protected $dates = ['deleted_at'];
}
