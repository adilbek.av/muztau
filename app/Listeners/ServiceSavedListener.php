<?php

namespace App\Listeners;

use App\Events\ServiceSaved;
use Illuminate\Support\Str;

class ServiceSavedListener
{
    public function handle(ServiceSaved $event)
    {
        $event->service->update([
            'slug' => Str::slug($event->service->title)
        ]);
    }
}
