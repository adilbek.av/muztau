<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Application extends Model
{
    protected $fillable = ['name', 'phone', 'email', 'message', 'type'];
    protected $hidden = ['updated_at', 'deleted_at'];
    protected $dates = ['deleted_at'];
}
