<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Resizable;


class Client extends Model
{
    use Resizable, SoftDeletes;
    protected $hidden = ['order', 'updated_at', 'deleted_at'];
    protected $dates = ['deleted_at'];
}
