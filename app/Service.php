<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Resizable;
use TCG\Voyager\Traits\Translatable;


class Service extends Model
{
    use Translatable, Resizable, SoftDeletes;
    protected $translatable = ['title', 'excerpt', 'description', 'slug'];
    protected $fillable = ['title', 'excerpt', 'description', 'images', 'top_image', 'bottom_image', 'order', 'slug'];
    protected $hidden = ['order', 'updated_at', 'deleted_at'];
    protected $dates = ['deleted_at'];

}
